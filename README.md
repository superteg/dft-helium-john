# Hur man kör detta projekt
    python3 dft.py

# Dependencies
För att köra detta projekt behöver man ha installerat matplotlib, numpy och scipy. 

    pip install matplotlib numpy scipy

När koden har kört klart skriver koden ut den helium grundtillståndsenergin i terminalen och plottar laddningsfördelningen.

