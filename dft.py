import numpy as np
import scipy.linalg as la
from scipy.integrate import simps
import matplotlib.pyplot as plt

def get_Vx(density):
    # Eventuellt använder man inte n_s här. Behöver multiplicera densiteten med 2
    double_density = 2*density
    eps_x = get_epsx(density)
    dedn = - (3/np.pi)**(1/3)/(4*double_density**(2/3))
    return eps_x + double_density * dedn

def get_epsx(density):
    double_density = 2*density
    #double_density = density
    eps_x = - 3/(4 * np.pi)*(3*np.pi**2 * double_density)**(1/3)
    return eps_x

def get_Vc(density):
    # Eventuellt använder man inte n_s här, behöver multiplicera densiteten med 2
    double_density = 2 * density
    #double_density = density
    y0 = -0.10498
    b = 3.72744
    c = 12.9352
    A = 0.00621814
    r_s = (3/(4*np.pi*double_density))**(1/3)
    y = np.sqrt(r_s)
    Y = lambda y: y**2 + b*y + c
    Q = np.sqrt(4*c - b**2)

    t1 = A/2 * np.log(y**2/Y(y))
    t2 = A*b/Q * np.arctan(Q/(2*y + b))
    t3 = - A * b * y0/(2*Y(y0)) * (np.log((y - y0)**2/(Y(y))) + 2*(b + 2*y0)/Q * np.arctan(Q/(2*y + b)))
    Vc = t1 + t2 +t3
    return Vc

def get_epsc(density):
    double_density = 2 * density
    #double_density = density

    y0 = -0.10498
    b = 3.72744
    c = 12.9352
    A = 0.00621814
    r_s = (3/(4*np.pi*double_density))**(1/3)
    y = np.sqrt(r_s)
    Y = lambda y: y**2 + b*y + c
    Q = np.sqrt(4*c - b**2)

    # Från dft_atom
    eps_x = A/2 * ( np.log(y**2/Y(y)) + 2*b/Q * np.arctan(Q/(2*y + b)) - b*y0/Y(y0)*(np.log((y - y0)**2/Y(y)) + 2*(b + 2*y0)/Q * np.arctan(Q/(2*y + b))))
    return eps_x

def get_hartree_potential(r_vector, density_func):
    u = np.sqrt(4*np.pi * density_func[1:-1])*r_vector[1:-1]

    # löser Ax=b
    # A är d^2/dr^2
    # x är sökt funktion U

    b = -u**2/r_vector[1:-1]
    d2dx2 = get_derivative_matrix(r_vector)

    U0 = np.linalg.solve(d2dx2, b)

    # Lägg in begynelsevillkor
    U0 = np.append(U0, 0)
    U0 = np.insert(U0, 0, 0)

    U = U0 + r_vector/np.max(r_vector)

    # U = r V_H
    V_H = U/r_vector

    # V_H = 2*V_sH
    V_H = 2*V_H

    # V_H är två kortare i längd i jämfört med r
    # Detta gör egentligen inte något, då dessa värden inte används
    # vid lösning av problemet

    return np.diag(V_H)

def get_derivative_matrix(r_vector):

    r_len = len(r_vector)

    # De första och sista r-värdena används inte
    # randvärdena är givna genom dimensionen på matrisen.
    matrix_dim = r_len - 2

    # Skapa en tom matris av rätt dimension.
    diff_problem_matrix = np.zeros((matrix_dim, matrix_dim))

    # Den första och sista raden är lite speciella, då
    # matrisen inte opererar på randvärdena hos funktionen.

    # Fyll i den första raden.
    diff_problem_matrix[0][0] = -2
    diff_problem_matrix[0][1] = 1

    # Fyll i den sista raden.
    diff_problem_matrix[matrix_dim-1][matrix_dim-1] = -2
    diff_problem_matrix[matrix_dim-1][matrix_dim - 2] = 1

    # Fyll i den diskreta derivatan över allt annars.
    for i in range(1,matrix_dim - 1):
        diff_problem_matrix[i][i-1] = 1
        diff_problem_matrix[i][i] = -2
        diff_problem_matrix[i][i+1] = 1

    # Derivatans definition delar med längden mellan x (det vill säga r) värdena.
    # Dela med dem.
    h = r_vector[1] - r_vector[0]
    return 1/h**2 * diff_problem_matrix

def get_external_potential(r_vector):
    # 2/r i diagonalen. Tar första och sist
    r = r_vector[1:-1]
    return np.diag(2/r)

def paste_first_and_last(vector):
    return np.insert(vector, [0,-1], [vector[0], vector[-1]])

def get_problem(r, n):
    d2dr2              = get_derivative_matrix(r)
    external_potential = get_external_potential(r)
    hartree_potential  = get_hartree_potential(r,n)
    hartree_potential = hartree_potential[1:-1, 1:-1]
    exhange_correlation_potential = np.diag(get_Vx(n) + get_Vc(n))
    exhange_correlation_potential = exhange_correlation_potential[1:-1, 1:-1]
    matrix_problem = (-1/2 * d2dr2 - external_potential
           + hartree_potential + exhange_correlation_potential)

    return matrix_problem

def get_energy(r_vector, u, density, eigenvalue, hartree_potential, exchange_correlation_potential, eps_xc):
    r = r_vector[1:]
    eigenvalue = np.real(eigenvalue)

    hartree_potential = np.diag(hartree_potential)
    integrand = - 2 * simps(u[1:]**2 * (1/2 * hartree_potential[1:] +  exchange_correlation_potential[1:] - eps_xc[1:]), r)
    energy = 2*eigenvalue + integrand

    return energy

def SCF(r_vector, density, energy, energy_threshold, i=0):
    print("antal iterationer: " + str(i))

    # Först och främst, hämta problemet
    matrix_problem = get_problem(r_vector, density)
    hartree_potential = get_hartree_potential(r_vector, density)
    eigvals, u_all = la.eig(matrix_problem)

    # Hitta index på det minsta reella egenvärdet
    real_eigs = eigvals[np.real(eigvals) == eigvals]
    index = np.argmin(real_eigs)
    u = u_all[:,index]

    u_tmp = np.insert(u, [0, len(u)], [0,0])
    # Normalisera u
    C = simps(u_tmp**2, x = r_vector)
    u_tmp /= np.sqrt(C)
    u = u_tmp

    # Hitta den nya fantastiska vågfunktionen
    phi = 1/np.sqrt(4. * np.pi) * u[1:]/r[1:]
    # Gör en fuling: kopierar näst första värdet. Hoppas det fungerar
    phi = np.insert(phi, 0, phi[0] / (1 - (r_vector[1] - r_vector[0]) * 2))
    new_density = np.abs(phi)**2

    exchange_correlation_potential = get_Vx(new_density) + get_Vc(new_density)
    exchange_correlation_potential[len(exchange_correlation_potential)-1] = 0
    eps_xc = get_epsx(new_density) + get_epsc(new_density)
    eps_xc[len(eps_xc)-1] = 0

    new_energy = get_energy(r_vector, u, new_density, real_eigs[index], hartree_potential, exchange_correlation_potential, eps_xc)

    # Säkerställ att densitetens längd förblir densamma.
    # Kopiera in de två senaste värdena.

    # Ifall differensen är mindre än threshold, då är vi klara
    if(np.abs(new_energy - energy)< energy_threshold):
        return (new_energy, new_density)
    else:
        i = i+1
        return SCF(r_vector, new_density, new_energy, energy_threshold, i)

r = np.linspace(0, 15, 1000)
n = np.exp(-r)**2/np.pi

eV = 1/27.211
threshold = 1e-5 * eV

(energy, density) = SCF(r, n, 0, threshold)

print("Calculated energy")
print(str(energy) + "[Ha]")

plt.plot(r, n)
plt.xlabel("Distans från kärnan, [$a_0$]")
plt.ylabel("Laddningstäthet, $n$, [$a_0^{-3}$]")
plt.show()
